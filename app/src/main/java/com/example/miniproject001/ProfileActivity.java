package com.example.miniproject001;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.Calendar;
import java.util.Objects;

public class ProfileActivity extends AppCompatActivity {
    String[] subjects = {"Android Developer","Web Developer","Java Developer","Game Developer","IOS Developer","Mobile Developer"};
    Spinner spinner;

    private int mYear, mMonth, mDay;
    TextView txt_date;
    Button btnDate,btnSave;
    RadioButton selectedRadioButton;

    String selectedRbText;

    Dialog dialogMessage;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDefaultDisplayHomeAsUpEnabled(true);


        dialogMessage = new Dialog(this);

        // set list view to spinner
        spinner = findViewById(R.id.spinner);
        ArrayAdapter spinnerView = new ArrayAdapter(this,android.R.layout.simple_spinner_item, subjects);
        spinnerView.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerView);

        // Create activity date
        btnDate = findViewById(R.id.btn_date);
        txt_date = findViewById(R.id.txt_view_date);

        btnDate.setOnClickListener(v -> {
            if (v == btnDate){
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(ProfileActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        txt_date.setText(dayOfMonth + " / " +(month+1) + " / " + year);
                    }
                },mYear,mMonth,mDay);
                datePickerDialog.show();
            }
        });

        // Get value from radioGroup
        final RadioGroup radioGroup = findViewById(R.id.radio_group);

        // Get value form text
        EditText et_text = findViewById(R.id.et_caption);
        final String caption = et_text.getText().toString();

        // Get value from spinner
        final String spinnerValue = spinner.getSelectedItem().toString();



        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(v -> {

            int selectedRadioButtonId = radioGroup.getCheckedRadioButtonId();
            if (selectedRadioButtonId != -1) {
                selectedRadioButton = findViewById(selectedRadioButtonId);
                selectedRbText  = selectedRadioButton.getText().toString();

            } else {
                selectedRbText  ="Nothing selected from the radio group";
            }


            new AlertDialog.Builder(ProfileActivity.this)
                    .setIcon(R.drawable.profile)
                    .setTitle("INFORMATION")
                    .setMessage("Caption : "+caption+"\nDate : "+txt_date.getText().toString()+"\nGender : "+selectedRbText+"\nJob :"+spinnerValue)
                    .setNegativeButton("Cancel", null)
                    .show();

        });
    }
}
