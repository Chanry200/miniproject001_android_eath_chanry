package com.example.miniproject001;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class NoteActivity extends AppCompatActivity {
    private GridLayout rootLayout;
    FrameLayout addItem;
    ViewDynamicActivity viewDynamic;
    Context context;
    int i =1,j=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        rootLayout = findViewById(R.id.controller_note);
        addItem = findViewById(R.id.add_item);

        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NoteActivity.this, AddActivity.class);
                startActivityForResult(intent,1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check if the request code is same as what is passed  here it is 1
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                final String title = data.getStringExtra("title");
                final String contact = data.getStringExtra("contact");
                viewDynamic = new ViewDynamicActivity(context);
                GridLayout.Spec column = GridLayout.spec(1);
                GridLayout.Spec row =  GridLayout.spec(0);
                rootLayout.addView(viewDynamic.frame(getApplicationContext(),title,contact),new GridLayout.LayoutParams(row,column));
//                display it in a Toast.
                Toast.makeText(this, "Title : "+title+"\n Contact : "+contact, Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this, "Result: no item", Toast.LENGTH_LONG).show();
            }
        }
    }
}
